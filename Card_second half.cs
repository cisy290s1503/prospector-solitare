using UnityEngine;
using System.Collections;

public class Card : MonoBehaviour {

	public CardDefinition def;

	// List of the SpriteRenderer Components of this GameObject and its children
	public SpriteRenderer[] spriteRenderers;

	// Use this for initialization
	void Start () {
		SetSortOrder (0);
	}
	
	public bool faceUp	{

	}

	// If spriteRenderers is not yet defined, this function defines it
	public void PopulateSpriteRenderers()	{
		// If spriteRenderers in null or empty
		if (spriteRenderers == null || spriteRenderers.Length == 0) {
			// Get SpriteRenderer Components of this GameObject and its children
			spriteRenderers = GetComponentsInChildren<SpriteRenderer> ();
		}
	}

	// Sets the sortingLayerName on all SpriteRenderer Components
	public void SetSortingLayerName(string tSLN)	{
		PopulateSpriteRenderers ();

		foreach (SpriteRenderer tSR in spriteRenderers)	{
			tSR.sortingLayerName = tSLN;
		}
	}

	// Set the sortingOrder of all SpriteRenderer Components
	public void SetSortOrder(int sOrd)	{
		PopulateSpriteRenderers();

		foreach (SpriteRenderer tSR in spriteRenderers)	{
			if (tSR.gameObject == this.gameObject)	{
				tSR.sortingOrder = sOrd;
				continue;
			}
			switch (tSR.gameObject.name)	{
			case "back":
				tSR.sortingOrder = sOrd+2;
				break;
			case "face":
			default:
				tSR.sortingOrder = sOrd+1;
				break;
			}
		}
	}

	virtual public void OnMouseUpAsButton()	{
		print (name);
	}

	override public void OnMouseUpAsButton()	{
		Prospector.S.CardClicked (this);
		base.OnMouseUpAsButton ();
	}
}