using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum ScoreEvent{
	draw,
	mine,
	mineGold,
	gameWin,
	gameLoss
}

public class Prospector : MonoBehaviour {
	static public Prospector S;
	static public int SCORE_FROM_PREV_ROUND = 0;
	static public int HIGH_SCORE = 0;

	public float reloadDelay = 1f;

	public Vector3 fsPosMid = new Vector3(0.5f, 0.90f, 0);
	public Vector3 fsPosRun = new Vector3(0.5f, 0.75f, 0);
	public Vector3 fsPosMid2 = new Vector3(0.5f, 0.5f, 0);
	public Vector3 fsPosEnd = new Vector3(1.0f, 0.65f, 0);

	public Deck deck;
	public TextAsset deckXML;

	public Layout layout;
	public TextAsset layoutXML;
	public Vector3 layoutCenter;
	public float xOffset = 3;
	public float yOffset = -2.5f;
	public Transform layoutAnchor;

	void Awake ()	{
		S = this;	// Set up a Singleton for Prospector
		// Check for a high score in PlayerPrefs
		if (PlayerPrefs.HasKey ("ProspectorHighScore")) {
			HIGH_SCORE = PlayerPrefs.GetInt ("ProspectorHighScore");
		}
		// Add the score from last round, which will be >0 if it was a win
		score += SCORE_FROM_PREV_ROUND;
		// And reset the SCORE_FROM_PREV_ROUND
		SCORE_FROM_PREV_ROUND = 0;

		GameObject go = GameObject.Find ("GameOver");
		if (go != null) {
			GTRoundResult = go.GetComponent<GUIText> ();
		}
		go = GameObject.Find ("RoundResult");
		if (go != null) {
			GTRoundResult = go.GetComponent<GUIText> ();
		}
		ShowResultGTs (false);

		go = GameObject.gameObject.Find ("HighScore");
		string hScore = "High Score: " + Utils.AddCommasToNumber (HIGH_SCORE);
		go.GetComponent<GUIText> ().text = hScore;
	}

	void ShowResultGTs(bool show)	{
		GTGameOver.gameObject.SetActive (show);
		GTRoundResult.gameObject.SetActive (show);
	}

	public CardProspector target;
	public List<CardProspector> tableau;
	public List<CardProspector> discardPile;

	public List<CardProspector> drawPile;

	// Fields to track score info
	public int chain = 0;
	public int scoreRun = 0;
	public int score = 0;
	public FloatingScore fsRun;

	public GUIText GTGameOver;
	public GUIText GTRoundResult;

	// Use this for initialization
	void Start () {
		Scoreboard.S.score = score;
		deck = GetComponent<Deck> ();	// Get the Deck
		deck.IntDeck (deckXML.text);	// Pass DeckXML to it
		Deck.Shuffle (ref deck.cards);


		layout = GetComponent<Layout> ();	// Get the Layout
		layout.ReadLayout (layoutXML.text);	// Pass LayoutXML to it
		drawPile = ConvertListCardsToListCardProspectors (deck.cards);
		LayoutGame ();
	}

	// The Draw function will pull a single card from the drawPile and return it
	CardProspector Draw()	{
		CardProspector cd = drawPile [0];
		drawPile.RemoveAt (0);
		return(cd);
	}

	// Convert from the layoudID int to the CardProspector with that ID
	CardProspector FindCardByLayoutID(int layoutID)	{
		foreach (CardProspector tCP in tableau)	{
			// Search through all cards in the tableau list<>
			it (tCP.layoutID == layoutID)	{
				return (tCP);
			}
		}
		// If it's not found, return null
		return (null);
	}

	// LayoutGame () positions the initial tableau of cards, a.k.a. the "mine"
	void LayoutGame ()	{
		// Create an empty GameObject to serve as an anchor for the tableau
		if (layoutAnchor == null) {
			GameObject tGO = new GameObject ("_LayoutAnchor");
			// ^ Create an empty GameObject named _LayoutAnchor in the Hierarchy
			layoutAnchor = tGO.transform;
			layoutAnchor.transform.position = layoutCenter;
		}

		CardProspector cp;
		// Follow the layout
		foreach (SlotDef tSD in layout.slotDefs) {
			// ^ Iterate through all the SlotDefs in the layout.slotDefs as tSD
			cp = Draw ();
			cp.faceUp = tSD.faceUp;
			cp.transform.parent = layoutAnchor;

			cp.transform.localPosition = new Vector3 (
				layout.multiplier.x * tSD.x,
				layout.multiplier.y * tSD.y,
				-tSD.layerID);
			// ^ Set the localPosition of the card based on slotDef
			cp.layoutID = tSD.id;
			cp.slotDef = tSD;
			cp.state = CardState.tableau;
			// CardProspectors in the tableau have the state CardState.tableau

			cp.SetSortingLayerName(tSD.layerName);	// Set the sorting layers

			tableau.Add (cp);
		}

		// Set which cards are hiding others
		foreach (CardProspector tCP in tableau) {
			foreach (int hid in tCP.slotDef.hiddenBy) {
				cp = FindCardByLayoutID (hid);
				tCP.hiddenBy.Add (cp);
			}
		}

		// Set up the target card
		MoveToTarget (Draw ());

		// Set up the Draw pile
		UpdateDrawPile ();
	}

	// CardClicked is called anytime a card in the game is clicked
	public void CardClicked(CardProspector cd)	{
		switch (cd.state)	{
		case CardState.target:
			break;
		case CardState.drawpile:
			MoveToDiscard(target);
			MoveToTarget(Draw());
			ScoreManager(ScoreEvent.draw);
			break;
		case CardState.tableau:
			bool validMatch = true;
			if (!cd.faceUp)	{
				validMatch = false;
			}
			if(!AdjacentRank(cd,target))	{
				validMatch = false;
			}
			if (!validMatch) return;
			tableau.Remove(cd);
			MoveToTarget(cd);
			SetTableauFaces();
			ScoreManager(ScoreEvent.draw);
			break;
		}
		// Check to see whether the game is over or not
		CheckForGameOver ();
	}

	// Moves the current target to the discardPile
	void MoveToDiscard(CardProspector cd)	{
		cd.state = CardState.discard;
		discardPile.Add (cd);
		cd.transform.parent = layerAnchor;
		cd.transform.localPosition = new Vector3 (
			layout.multiplier.x * layout.discardPile.x,
			layout.multiplier.y * layout.discardPile.y,
			-layout.discardPile.layerID + 0.5f);
		// ^ Position it on the discardPile
		cd.faceUp = true;
		// Place it on top of the pile for depth sorting
		cd.SetSortingLayerName (layout.discardPile.layerName);
		cd.SetSortOrder (-100 + discardPile.Count);
	}

	// Make cd the new target card
	void MoveToTarget(cardProspector cd)	{
		if (target != null)
			MoveToDiscard (target);
		target = cd;
		cd.state = CardState.target;
		cd.transform.parent = layoutAnchor;
		// Move to the target position
		cd.transform.localPosition = new Vector3 (
			layout.multiplier.x * layout.discardPile.x,
			layout.multiplier.y * layout.discardPile.y,
			-layout.discardPile.layerID);
		cd.faceUp = ture;
		// Set the depth sorting
		cd.SetSortingLayerName (layout.discardPile.layerName);
		cd.SetSortOrder (0);
	}

	// Arrange all the cards of the drawPile to show how many are left
	void UpdateDrawPile()	{
		CardProspector cd;
		for (int i=0; i<drawPile.Count; i++) {
			cd = drawPile [i];
			cd.transform.parent = layoutAnchor;
			// Position it correctly with the layout.drawPile.stagger
			Vector2 dpStagger = layout.drawPile.stagger;
			cd.transform.localPosition = new Vector3 (
				layout.multiplier.x * (layout.drawPile.x + i * dpStagger.x),
				layout.multiplier.y * (layout.drawPile.y + i * dpStagger.y),
				-layout.drawPile.layerID + 0.1f * i);
			cd.faceUp = false;
			cd.state = CardState.drawpile;
			// Set depth sorting
			cd.SetSortingLayerName (layout.drawPile.layerName);
			cd.SetSortOrder (-10 * i);
		}
	}
	
	List<CardProspector> ConvertListCardsToListCardProspectors(List<Card>1CD)	{
		List<CardProspector> 1CP = new List<CardProsepctor>();
		CardProspector tCP;
		foreach (Card tCD in 1CD)	{
			tCP = tCD as CardProspector;
			1CP.Add(tCP);
		}
		return(1CP);
	}

	public bool AdjacentRank(CardProspector c0, CardProspector c1)	{
		if (!c0.faceUp || !c1.faceUp)
			return(false);
		if (Mathf.Abs (c0.rank - c1.rank) == 1) {
			return(true);
		}
		if (c0.rank == 1 && c1.rank == 13)
			return(true);
		if (c0.rank == 13 && c1.rank == 1)
			return (true);

		// Otherwise, return false
		return (false);
	}

	// This turns cards in the Mine face-up or face-down
	void SetTableauFaces()	{
		foreach(CardProspector cd in tableau)	{
			bool fup = true;
			foreach (CardProspector cover in cd.hiddenBy)	{
				if (cover.state == CardState.tableau)	{
					fup = false;
				}
			}
			cd.faceUp = fup;
		}
	}

	// Test whether the game is over
	void CheckForGameOver()	{
		// If the tableau is empty, the game is over
		if (tableau.Count == 0) {
			// Call GameOver() with a win
			GameOver (true);
			return;
		}

		// If there are still cards in the draw pile, the game's not over
		if (drawPile.Count > 0) {
			return;
		}
		// Check for remaining valid plays
		foreach (CardProspector cd in tableau) {
			if (AdjacentRank (cd, target)) {
				return;
			}
		}
		// Since there are no valid plays, the game is over
		// Call GameOver with a loss
		GameOver (false);
	}

	// Called when the game is over. Simple for now, but expandable
	void GameOver(bool won)	{
		if (won)	{
			ScoreManager(ScoreEvent.gameWin);
		}	else {
			ScoreManager(ScoreEvent.gameLoss);
		}
		// Reload the scene in reloadDelay seconds
		Invoke ("ReloadLevel", reloadDelay);
		// Application.LoadLevel("_Prospector_Scene_0");
	}

	void ReloadLevel()	{
		Application.LoadLevel ("_Prospector_Scene_0");
	}

	// ScoreManager handles all of the scoring
	void ScoreManager(ScoreEvent sEvt)	{
		List<Vector3> fsPts;
		switch (sEvt)	{
			// Same things need to happen whether it's a draw, a win, or a loss
		case ScoreEvent.draw:
		case ScoreEvent.gameWin:
			chain = 0;
			score += scoreRun;
			scoreRun = 0;
			if (fsRun != null)	{
				fsPts = new List<Vector3>();
				fsPts.Add(fsPosRun);
				fsPts.Add(fsPosMid2);
				fsPts.Add(fsPosEnd);
				fsRun.reportFinishTo = Scoreboard.S.gameObject;
				fsRun.Init(fsPts, 0, 1);
				fsRun.fontSizes = new List<float>(new float[] {28,36,4});
				fsRun = null;
			}
			break;
		case ScoreEvent.mine:
			chain ++;
			scoreRun += chain;
			FloatingScore fs;
			Vector3 p0 = Input.mousePosition;
			p0.x /= Screen.width;
			p0.y /= Screen.height;
			fsPts = new List<Vector3>();
			psPts.Add(p0);
			fspts.Add(fsPosMid);
			fsPts.Add(fsPosRun);
			fs = Scoreboard.S.CreatefloatingScore(chain,fsPts);
			fs.fontSizes = new List<float>(new float[] {4,50,28});
			if (fsRun == null)	{
				fsRun = fs;
				fsRun.reportFinishTo = fsRun.gameObject;
			}
			break;
		}

		// This second witch statement handles round wins and losses
		switch (sEvt){
		case ScoreEvent.gameWin:
			GTGameOver.text = "Round Over";
			Prospector.SCORE_FROM_PREV_ROUND = score;
			print ("You won this round! Round score: "+score);
			GTRoundResult.text = "You won this round:\nRound Score: "+score;
			showResultGTs(true);
			break;
		case ScoreEvent.gameLoss:
			GTGameOver.text = "Game Over";
			if (Prospector.HIGH_SCORE <= score)	{
				print("You got the high score! High score: "+score);
				string sRR = "You got the high score!\nHigh score: "+score;
				GTRoundResult.text = sRR;
				Prospector.HIGH_SCORE = score;
				PlayerPrefs.SetInt("ProspectorHighScore", score);
			}	else {
				print ("Your final score for the game was: "+score);
				GTRoundResult.text = "Your final score was: "+score;
			}
			ShowResultGTs(true);
			break;
		}
	}
}
