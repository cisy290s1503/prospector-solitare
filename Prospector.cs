﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Prospector : MonoBehaviour {
	static public Prospector 	S;
	public Deck					deck;
	public TextAsset			deckXML;
	
	public Layout 				layout;
	public TextAsset			layoutXML;


	void Awake() {
		S = this; // Set up a Singleton for Pospector
	}

	// Use this for initialization
	void Start () {
		deck = GetComponent<Deck> (); // Get the Deck
		deck.InitDeck (deckXML.text); // Pass DeckXML to it 
		
		layout = GetComponent<Layout>();
		layout.ReadLayout(layoutXML.text);
	}
}
